import sys
import math
import random
import pygame

RESOLUTION = (1280, 720)
CENTER = (RESOLUTION[0] / 2, RESOLUTION[1]/2)


PLAYERSIZE = (32, 32)
PLAYER_SPEED = 3
PLAYER_JUMP_SPEED = 8
GRAVITY = 4
GRAVITY_EFFECT = 0.2

ENEMYSIZE = (32, 32)
ENEMY_SPEED = PLAYER_SPEED

BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
YELLOW = (255, 255, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
ORANGE = (255, 165, 0)


pygame.init()
surface = pygame.display.set_mode(RESOLUTION)
game_clock = pygame.time.Clock()
large_font = pygame.font.Font(None,32)
default_font = pygame.font.Font(None,24)
small_font = pygame.font.Font(None,16)

player_group = pygame.sprite.GroupSingle()
wall_group = pygame.sprite.Group()
enemy_group = pygame.sprite.Group()


class GravitySprite(pygame.sprite.Sprite):
    def __init__(self, surface: pygame.Surface, gravity: float, gravity_increment: float, structures: pygame.sprite.Group,\
        x_pos:float = 50, y_pos:float = 50, width = 50, height = 50, color = ORANGE, *args, **kwargs):
        super(GravitySprite,self).__init__(*args,**kwargs)
        self.surface = surface
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.width = width
        self.height = height
        self.color = color
        self.image = pygame.Surface((width, height))
        self.image.fill(color)
        self.rect = self.image.get_rect(top = y_pos, left = x_pos)
        self.gravity = gravity # Maximum speed that can be achieved through gravity.
        self.gravity_increment = gravity_increment # speed increments to reach gravity_increment.
        self.structures = structures # platforms
        self.velocity_x = 0 # left and right velocity: right is positive.
        self.velocity_y = 0 # upwards and downwards velocity: down is positive.
        self.apply_gravity = True
    
    @property
    def on_platform(self):
        bottom_rect = self.rect.copy()
        border_size = max(abs(self.velocity_y),3)
        bottom_rect.height = border_size+1
        bottom_rect.y = self.rect.bottom-border_size
        for structure in self.structures.sprites():
            if self.velocity_y >= 0 and bottom_rect.colliderect(structure): 
                self.rect.bottom = structure.rect.top
                return True
        return False

    @property
    def jump_obstructed(self):
        top_rect = self.rect.copy()
        border_size = max(self.velocity_y,3)
        top_rect.height = border_size
        top_rect.y = self.rect.y-border_size

        for structure in self.structures.sprites():
            if top_rect.colliderect(structure) and structure.solid_sides & Wall.BOTTOM: return True

        return False
    
    @property
    def left_obstructed(self):
        left_rect = self.rect.copy()
        border_size = max(abs(self.velocity_x),3)
        left_rect.x = self.rect.x - border_size
        left_rect.width = border_size

        for structure in self.structures.sprites():
            if left_rect.colliderect(structure):
                return True
        return False
    
    @property
    def right_obstructed(self):
        right_rect = self.rect.copy()
        border_size = max(self.velocity_x,3)
        right_rect.x = self.rect.right
        right_rect.width = border_size

        for structure in self.structures.sprites():
            if right_rect.colliderect(structure):
                return True
        return False

    def update(self):
        if self.on_platform and self.velocity_y > 0: # removed = sign
            self.velocity_y = 0
        elif self.apply_gravity: self.velocity_y += self.gravity_increment if self.velocity_y < self.gravity else 0

        if self.left_obstructed and self.velocity_x < 0:
            self.velocity_x = 0

        if self.right_obstructed and self.velocity_x > 0:
            self.velocity_x = 0

        self.rect.bottom += self.velocity_y
        self.draw()

    def draw(self):
        self.surface.blit(self.image,self.rect)


class Wall(pygame.sprite.Sprite):
    HORIZONTAL_THRESHOLD = 20
    VERTICAL_THRESHOLD = 20

    TOP = 0b0001
    BOTTOM = 0b0010
    LEFT = 0b0100
    RIGHT = 0b1000
    ALL_SIDES = 0b1111

    def __init__(self,x_pos,y_pos,width,height, color, solid_sides = ALL_SIDES):
        super(Wall,self).__init__()
        self.image = pygame.surface.Surface((width, height))
        self.image.fill(color)
        self.rect = self.image.get_rect(top = y_pos, left = x_pos)
        self.top_rect = self.rect.copy()
        self.top_rect.height = 3
        self.solid_sides = solid_sides
    
    @property
    def is_horizontal(self):
        return (self.rect.bottom - self.rect.top) < Wall.HORIZONTAL_THRESHOLD

    @property
    def is_vertical(self):
        return (self.rect.right - self.rect.left) < Wall.VERTICAL_THRESHOLD

    def update(self):
        surface.blit(self.image,self.rect)


class Player(GravitySprite):
    JUMP_DURATION_MS = 500
    '''LANDED = 0
    JUMPING = 1
    FALLING = 2'''


    def __init__(self, x_pos, y_pos):
        super(Player,self).__init__(surface,GRAVITY,GRAVITY_EFFECT,wall_group, x_pos, y_pos, PLAYERSIZE[0], PLAYERSIZE[1], YELLOW)
        #self.image = pygame.surface.Surface(PLAYERSIZE)
        self.image.fill(BLACK)
        self.image.convert_alpha()
        self.image.set_colorkey(BLACK)
        pygame.draw.polygon(self.image,self.color, [(0, 32), (16, 0), (32, 32)])

        # alternative images
        self.image_up = self.image.copy()
        self.image_left = pygame.transform.rotate(self.image_up,90)
        self.image_down = pygame.transform.rotate(self.image_up,180)
        self.image_right = pygame.transform.rotate(self.image_up,270)

        #self.vertical_state = Player.LANDED
        self.last_jump = pygame.time.get_ticks()
        self.velocity_x = 0
        self.velocity_y = 0

    @property
    def can_jump(self):
        ''' return true if:
        - the player is on a platform
        - jumping is not on cooldown.
        '''
        return self.on_platform and pygame.time.get_ticks() >= (self.last_jump + Player.JUMP_DURATION_MS)

    def register_jump(self):
        self.last_jump = pygame.time.get_ticks()


    def move_y(self,speed):
        if speed > 0 and self.rect.bottom <= (RESOLUTION[1] - speed): self.rect.y += speed
        elif speed < 0 and self.rect.top >= speed: self.rect.y += speed

    def move_x(self,speed):
        if speed > 0 and self.rect.right <= (RESOLUTION[0] - speed): self.rect.x += speed
        elif speed < 0 and self.rect.left >= -speed: self.rect.x += speed


    def update(self):
        super().update()
        if self.rect.y > RESOLUTION[1] + 10:
            self.rect.y = -10

        keys = pygame.key.get_pressed()

        if keys[pygame.K_LEFT]:
            if not self.left_obstructed:
                self.velocity_x = -PLAYER_SPEED
            self.image = self.image_left
        elif keys[pygame.K_RIGHT]:
            if not self.right_obstructed:
                self.velocity_x = PLAYER_SPEED
            self.image = self.image_right
        else:
            self.velocity_x = 0
            self.image = self.image_up

        if keys[pygame.K_UP]:
            if self.can_jump:
                self.velocity_y = -PLAYER_JUMP_SPEED
                self.register_jump()
        self.move_x(self.velocity_x)

        if self.velocity_y < 0:
            if self.jump_obstructed: self.velocity_y = 0


class BasicEnemy(GravitySprite):
    def __init__(self, surface: pygame.Surface, gravity: float, gravity_increment: float, structures: pygame.sprite.Group,\
        x_pos:float, y_pos:float,width: int, height: int, color = RED, starting_direction = -1):
        super(BasicEnemy,self).__init__(surface, gravity, gravity_increment, structures, x_pos, y_pos, width, height, color)
        self.direction = starting_direction
        self.image = pygame.Surface((width,height))
        self.image.convert_alpha()
        self.image.set_colorkey(BLACK)
        pygame.draw.circle(self.image,color,(math.floor(width/2),math.floor(height/2)),math.floor(width/2))
        
    def update(self):
        super().update()
        if self.direction < 0:
            if not self.left_obstructed: self.rect.x -= ENEMY_SPEED
            else: self.direction *= -1
        if self.direction > 0:
            if not self.right_obstructed: self.rect.x += ENEMY_SPEED
            else: self.direction *= -1


def setup():
    global gravitysprite

    player_group.empty()
    enemy_group.empty()
    wall_group.empty()

    player_group.add(Player(*CENTER))

    enemy_group.add(BasicEnemy(surface,GRAVITY,GRAVITY_EFFECT,wall_group,CENTER[0],RESOLUTION[1]-50,ENEMYSIZE[0],ENEMYSIZE[1],RED,-1))

    gravitysprite = GravitySprite(surface,3,0.25,wall_group, x_pos = 25, y_pos = 0)

    for i in range(150):
        x = 1+1*i
        y = RESOLUTION[1] - (1+1*i)
        wall = Wall(x,y,5,5,GREEN,Wall.TOP)
        wall_group.add(wall)


    for i,x in enumerate(range(0,RESOLUTION[0],150)):
        for j,y in enumerate(range(RESOLUTION[1]-150,0,-150)):
            solid_bottom = (i+j) % 3
            solid_sides = Wall.TOP | Wall.BOTTOM
            wall = Wall(x,y,50,5,GREEN if not solid_bottom else RED, solid_sides if solid_bottom else Wall.TOP )
            wall_group.add(wall)
    wall_group.add(Wall(CENTER[0]-128,RESOLUTION[1]-32,5,32,BLUE,Wall.TOP))
    wall_group.add(Wall(CENTER[0]+128,RESOLUTION[1]-32,5,32,BLUE,Wall.TOP))
    wall_group.add(Wall(0,RESOLUTION[1],RESOLUTION[0],3,RED))


def draw_debug():
    player_x_pos_str = "Player X pos: {:.02f}".format(player_group.sprite.rect.x)
    player_y_pos_str = "Player Y pos: {:.02f}".format(player_group.sprite.rect.y)
    player_x_str = "Player X speed: {:.02f}".format(player_group.sprite.velocity_x)
    player_y_str = "Player Y speed: {:.02f}".format(player_group.sprite.velocity_y)
    player_on_platform = "Player on stucture: {}".format(player_group.sprite.on_platform)
    player_left_obstructed = "Player left obstructed: {}".format(player_group.sprite.left_obstructed)
    player_right_obstructed = "Player right obstructed: {}".format(player_group.sprite.right_obstructed)
    
    render_strings = [player_x_pos_str, player_y_pos_str, player_x_str, player_y_str, player_on_platform, player_left_obstructed, player_right_obstructed]

    for line,string in enumerate(render_strings):
        line_image =  small_font.render(string,True,WHITE)
        line_rect = line_image.get_rect(right=RESOLUTION[0] - 6, top = 6 + line*16)
        surface.blit(line_image,line_rect)



def main():

    setup()

    show_debug = False

    while True:

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    sys.exit()
                if event.key == pygame.K_r:
                    setup()
                if event.key == pygame.K_g:
                    player_group.sprite.apply_gravity = not player_group.sprite.apply_gravity
                if event.key == pygame.K_F3:
                    show_debug = not show_debug

        surface.fill(BLACK)

        if show_debug: draw_debug()
        wall_group.update()
        enemy_group.update()
        gravitysprite.update()
        player_group.update()

        pygame.display.update()

        game_clock.tick(120)

if __name__ == "__main__":
    main()

