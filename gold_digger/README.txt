Gold Digger Game

Made by Celerate while learning how to use PyGame

To run this game you need Python 3. The game was tested with 3.8 and 3.9; however, it will probably work with earlier versions that are
close. To run the game you'll need to install PyGame, either using `pip install --user pygame` or your operating system's package
manager.

The game is simple - click on a square and find out if you've found the gold. If not, you'll see your distance from the gold in the bottom
left to give you a hint. The number of tries you've made to find the gold is shown on the bottom right. When you find the gold the game
will pause and a pop-up will let you know how many tries it took you to find it.

To restart the game at any point you can press the 'r' key on your keyboard. To exit the game you can press 'ESC' on your keyboard, or just
close the window.

