import sys
import random
from math import sqrt
from pathlib import Path
# third party imports
import pygame

ROOT_PATH = Path(__file__).parent

# constants
RESOLUTION = (1280,720)
CENTER = (RESOLUTION[0]/2,RESOLUTION[1]/2)
V_PADDING = 40
H_PADDING = 40
H_TILES = 25
V_TILES = 13
TILE_SIZE = 48

# end screen parameters
END_SCREEN_W = 560
END_SCREEN_H = 120



# Game States
LAUNCHED = -1
IN_PROGRESS = 0
WON = 1
LOST = 2

# colour constants
WHITE = (255,255,255)
ALMOND = (239, 222, 205)
AERO_BLUE = (124, 185, 232)
MEDIUM_ELECTRIC_BLUE  = (0, 77, 153)
CORN_YELLOW = (255, 230, 102)
OXFORD_BLUE = (0,34,51)
BLACK = (0,0,0)

# text
HEADER = "Press R to restart, or ESC to exit"
DISTANCE = "Distance: {:.02f}"
TRIES = "Tries: {}"
WON_TEXT = "You found the gold in {} tries."

TILES = {
    'grass': pygame.image.load(f'{ROOT_PATH}/art/grass.png'),
    'hole': pygame.image.load(f'{ROOT_PATH}/art/hole.png'),
    'gold': pygame.image.load(f'{ROOT_PATH}/art/gold.png')
}   

# setup
pygame.init()
display = pygame.display.set_mode(RESOLUTION)
pygame.display.set_icon(TILES['gold'])
pygame.display.set_caption("Gold digger")
game_clock = pygame.time.Clock()
large_font = pygame.font.Font(None,60)
medium_font = pygame.font.Font(None,40)
small_font = pygame.font.Font(None,26)


# game globals
gold_tile = (0,0)
last_guess = (0,0)
tries = 0
game_state = LAUNCHED
last_distance = 0



def tile_at(x_pos,y_pos):
    ''' return the tile index at the given position '''
    x_pos -= H_PADDING
    y_pos -= V_PADDING
    x_pos -= x_pos % TILE_SIZE
    y_pos -= y_pos % TILE_SIZE
    return (int(x_pos/TILE_SIZE), int(y_pos/TILE_SIZE))


def calculate_distance(point1, point2):
    ''' Calculate the distance in tiles from point1 to point2 '''

    x_diff = abs(point1[0] - point2[0])
    y_diff = abs(point1[1] - point2[1])
    distance = sqrt(x_diff**2 + y_diff**2)
    return distance


# respond to events.
def on_quit():
    ''' clean up behind our game and exit '''
    pygame.quit()
    sys.exit()


def display_info():
    ''' display the header text, the last guess' distance from the gold, and the number of tries so far. '''    
    header = medium_font.render(HEADER, True, ALMOND)
    header_rect = header.get_rect(center = (CENTER[0],20))
    display.blit(header,header_rect)

    distance = small_font.render(DISTANCE.format(last_distance), True, CORN_YELLOW)
    distance_rect = distance.get_rect(left = 10, bottom = RESOLUTION[1]-10)
    display.blit(distance,distance_rect)

    tries_ = small_font.render(TRIES.format(tries), True, CORN_YELLOW)
    tries_rect = tries_.get_rect(right = RESOLUTION[0]-10, bottom = RESOLUTION[1]-10)
    display.blit(tries_,tries_rect)



def display_won():
    ''' Display the won screen. ''' 
    container = pygame.Surface((END_SCREEN_W,END_SCREEN_H))
    container_rect = container.get_rect(center = CENTER)
    container.fill(BLACK)
    
    won_title = medium_font.render(WON_TEXT.format(tries),True,ALMOND)
    won_title_rect = won_title.get_rect(center = (END_SCREEN_W/2,END_SCREEN_H/2))
    container.blit(won_title,won_title_rect)

    display.blit(container,container_rect)



class GroundSprite(pygame.sprite.Sprite):
    
    def __init__(self,x_pos,y_pos,is_gold = False):
        ''' Set the sprites image, rect, position, and whether it has the gold. '''
        super(GroundSprite,self).__init__()
        self.image = TILES['grass']
        self.rect = self.image.get_rect(left = x_pos, top = y_pos)
        self.x_pos = x_pos
        self.y_pos = y_pos
        self.is_gold = is_gold

    
    def update(self):
        display.blit(self.image,self.rect)

    def tried(self):
        ''' has this tile been checked already? '''
        return self.image != TILES['grass']

    def on_click(self):
        ''' when clicked, change appearance and return whether or not this sprite had the gold. '''
        if self.is_gold:
            self.image = TILES['gold']
        else:
            self.image = TILES['hole']
        return self.is_gold


ground_sprites = pygame.sprite.Group()

def setup_game():
    ''' initialize or reset variables relating to the game state. '''
    global tries, game_state, last_guess, gold_tile
    
    # set/reset variables
    ground_sprites.empty() # remove any old sprites from the collection.
    tries = 0
    game_state = IN_PROGRESS
    last_guess = (0,0)
    gold_tile = ( # pick a random gold tile.
        random.randrange(0, H_TILES),
        random.randrange(0,V_TILES)
    )
    
    # generate the board's sprites.
    for i in range(H_TILES):
        current_x_pos = H_PADDING + (TILE_SIZE * i)
        for j in range(V_TILES):
            current_y_pos = V_PADDING + (TILE_SIZE * j)
            is_gold = tile_at(current_x_pos,current_y_pos) == gold_tile
            ground_sprites.add(GroundSprite(current_x_pos,current_y_pos,is_gold))


def game_loop():
    ''' main game logic '''
    display.fill(OXFORD_BLUE)
    display_info()
    ground_sprites.update()
    if game_state == WON:
        display_won()



setup_game()
# loop
while True:
    # handle events.
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                on_quit()
            elif event.key == pygame.K_r:
                setup_game()

        if event.type == pygame.QUIT:
            on_quit()

        if event.type == pygame.MOUSEBUTTONDOWN and game_state == IN_PROGRESS:
            for tile in ground_sprites:
                if tile.rect.collidepoint(event.pos) and not tile.tried():
                    last_distance = calculate_distance(tile_at(*event.pos),gold_tile)
                    tries += 1
                    print(tries)
                    game_state = WON if tile.on_click() else game_state
                    

    # run the game's loop.
    game_loop()
    
    pygame.display.update()
    game_clock.tick(120)
